# DeFiant NEO #



### What is DeFiant NEO? ###

**DeFiant NEO** is a mobile and progressive web dApp that provides fiat and decentralized financial services 
to novice and experienced **Neo** blockchain users via an uncomplicated GUI.

Blockchain technologies and cryptocurrency are extremely intimidating and difficult for new users to access.

However, everyone has a mobile phone.

DeFiant provides a simple graphic interface that allows novices and experts to easily enter the world of DeFi by way of their mobile phones and numbers.

The key component of DeFiant is an interactive GUI wizard that performs the following functions:

* Sets up a Neo wallet that serves as a fund repository and DeFiant account with a human-readable address

* Fund access and transfer via cell phone number

* Generates smart contracts that utilize RPC API, NeoVM, NeoFS, and Neo Smart Contract API to perform advanced services like token swapping in liquidity pools and compound interest calculations